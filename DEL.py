import board
import digitalio
import time

# ligne 6-9 : attenuer l'intensite de la diode de la carte
import adafruit_dotstar
ledin = adafruit_dotstar.DotStar(board.APA102_SCK, board.APA102_MOSI, 1)
ledin.brightness = 0.01
ledin[0] = (0, 255, 0) # vert

# controle de la diode du montage
led = digitalio.DigitalInOut(board.D1)
led.direction = digitalio.Direction.OUTPUT

while True:
    led.value = True
    time.sleep(0.5)
    led.value = True
    time.sleep(0.5)